package br.com.ozeano.curso.api.bb.domain.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Banco extends BaseEntity {

	private String codigo;
	private String nome;
	
}
